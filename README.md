# Flectra Community / l10n-spain

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[payment_redsys](payment_redsys/) | 2.0.2.0.2| Payment Acquirer: Redsys Implementation
[l10n_es_facturae_face](l10n_es_facturae_face/) | 2.0.1.2.0| Envío de Facturae a FACe
[l10n_es_partner](l10n_es_partner/) | 2.0.1.0.2| Adaptación de los clientes, proveedores y bancos para España
[l10n_es_vat_book](l10n_es_vat_book/) | 2.0.2.1.1| Libro de IVA
[l10n_es_toponyms](l10n_es_toponyms/) | 2.0.1.0.0| Topónimos españoles
[l10n_es_aeat_mod115](l10n_es_aeat_mod115/) | 2.0.1.0.1| AEAT modelo 115
[l10n_es_aeat_sii_oss](l10n_es_aeat_sii_oss/) | 2.0.1.0.0| Suministro Inmediato de Información en el IVA: OSS
[l10n_es_account_banking_sepa_fsdd](l10n_es_account_banking_sepa_fsdd/) | 2.0.1.0.0| Account Banking Sepa - FSDD (Anticipos de crédito)
[l10n_es_facturae_efact](l10n_es_facturae_efact/) | 2.0.1.1.0| Envío de Facturae a e.FACT
[l10n_es_irnr](l10n_es_irnr/) | 2.0.1.0.0| Retenciones IRNR (No residentes)
[l10n_es_aeat_mod390_oss](l10n_es_aeat_mod390_oss/) | 2.0.1.0.0| AEAT modelo 390 - OSS
[l10n_es_facturae](l10n_es_facturae/) | 2.0.2.2.0| Creación de Facturae
[l10n_es_facturae_sale_stock](l10n_es_facturae_sale_stock/) | 2.0.1.0.0| Entregas en Factura-e
[l10n_es_aeat_mod303_oss](l10n_es_aeat_mod303_oss/) | 2.0.1.0.0| AEAT modelo 303 - OSS
[l10n_es_aeat_mod349](l10n_es_aeat_mod349/) | 2.0.1.2.0| AEAT modelo 349
[l10n_es_aeat_partner_check](l10n_es_aeat_partner_check/) | 2.0.1.1.0| AEAT - Comprobación de Calidad de datos identificativos
[l10n_es_pos](l10n_es_pos/) | 2.0.3.0.0| Punto de venta adaptado a la legislación española
[l10n_es_aeat_mod303](l10n_es_aeat_mod303/) | 2.0.3.2.1| AEAT modelo 303
[l10n_es_vat_book_oss](l10n_es_vat_book_oss/) | 2.0.1.0.0| Libro de IVA OSS
[l10n_es_aeat_mod123](l10n_es_aeat_mod123/) | 2.0.1.0.0| AEAT modelo 123
[l10n_es_account_asset](l10n_es_account_asset/) | 2.0.1.0.0| Gestión de activos fijos para España
[l10n_es_partner_mercantil](l10n_es_partner_mercantil/) | 2.0.1.0.0| Añade los datos del registro mercantil a la empresa
[l10n_es_aeat_mod347](l10n_es_aeat_mod347/) | 2.0.2.1.1| AEAT modelo 347
[l10n_es_location_nuts](l10n_es_location_nuts/) | 2.0.1.0.0| NUTS specific options for Spain
[l10n_es_dua](l10n_es_dua/) | 2.0.1.1.0| Importaciones con DUA
[l10n_es_aeat_sii_oca](l10n_es_aeat_sii_oca/) | 2.0.2.2.0| Suministro Inmediato de Información en el IVA
[l10n_es_ticketbai_api](l10n_es_ticketbai_api/) | 2.0.2.3.2| TicketBAI - API
[l10n_es_ticketbai](l10n_es_ticketbai/) | 2.0.2.6.2| Declaración de todas las operaciones de venta realizadas por las personas y entidades que desarrollan actividades económicas
[l10n_es_mis_report](l10n_es_mis_report/) | 2.0.1.3.1| Plantillas MIS Builder para informes contables españoles
[l10n_es_dua_sii](l10n_es_dua_sii/) | 2.0.1.1.1| Suministro Inmediato de Información de importaciones con DUA
[l10n_es_aeat_mod111](l10n_es_aeat_mod111/) | 2.0.1.1.0| AEAT modelo 111
[delivery_dhl_parcel](delivery_dhl_parcel/) | 2.0.1.4.3| Delivery Carrier implementation for DHL Parcel using their API
[l10n_es_intrastat_report](l10n_es_intrastat_report/) | 2.0.2.1.0| Spanish Intrastat Product Declaration
[delivery_gls_asm](delivery_gls_asm/) | 2.0.1.4.1| Delivery Carrier implementation for GLS with ASMRed API
[l10n_es_aeat_mod390](l10n_es_aeat_mod390/) | 2.0.2.1.0| AEAT modelo 390
[l10n_es_aeat](l10n_es_aeat/) | 2.0.2.2.0| Modulo base para declaraciones de la AEAT
[l10n_es_account_statement_import_n43](l10n_es_account_statement_import_n43/) | 2.0.1.0.2| Importación de extractos bancarios españoles (Norma 43)
[l10n_es_aeat_vat_prorrate_asset](l10n_es_aeat_vat_prorrate_asset/) | 2.0.1.0.0| AEAT - Prorrata de IVA - Extensión para los activos
[l10n_es_aeat_vat_prorrate](l10n_es_aeat_vat_prorrate/) | 2.0.1.0.0| AEAT - Prorrata de IVA


